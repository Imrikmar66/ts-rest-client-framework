import '../scss/styles';
import { ModelService } from './services/ModelService';
import { Model, ModelLiteral } from './classes/Model';

interface PostLiteral extends ModelLiteral {
    title: string;
    content: string;
    image: string;
}
class Post extends Model {

    protected title: string;
    protected content: string;
    protected image: string;

    constructor( literal: PostLiteral ) {
        super( literal );
        this.title = literal.title;
        this.content = literal.content;
        this.image = literal.image;
    }
    
}

class PostService extends ModelService<PostLiteral, Post> {}
const service = new PostService(Post);

service.all().then( console.log ).catch( console.log );
service.find(7).then( console.log ).catch( console.log );
// service.delete( 6 ).then(console.log);

const $formA = <HTMLFormElement>document.getElementById('a');
$formA.addEventListener('submit', (e) => {
    
    e.preventDefault();
    service.store( $formA ).then( console.log );

});

const $formB = <HTMLFormElement>document.getElementById('b');
$formB.addEventListener('submit', (e) => {

    e.preventDefault();
    service.update( 1, $formB ).then( console.log );

});