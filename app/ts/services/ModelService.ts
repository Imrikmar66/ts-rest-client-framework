import { ModelLiteral, Model } from "../classes/Model";

export interface ModelResponseArray<T extends ModelLiteral[]> extends Response {
    json(): Promise<T>;
}
export interface ModelResponse<T extends ModelLiteral> extends Response {
    json(): Promise<T>;
}

export interface ModelFormData<T extends ModelLiteral> extends FormData {
    append(name: Extract<keyof T, string>, value: string | Blob, fileName?: string): void;
}

export abstract class ModelService<K extends ModelLiteral, T extends Model> {

    /**
     * @static
     * @readonly
     * @description the global url for each child services
     */
    static readonly api = 'http://laracms.vhost/api/';

    /**
     * @protected
     * @description The base url for requests
     */
    protected base_url: string;

    /**
     * @protected
     * @description model to use in service
     */
    protected blueprint: new ( data:K ) => T;

    /**
     * @constructor
     * @type new ( data:K ) => T
     * @param blueprint the model to use in service
     */
    constructor( blueprint: new ( data:K ) => T ) {
        this.blueprint = blueprint;
        if( !this.base_url ) this.base_url = ModelService.api + blueprint.name.toLowerCase() + 's';
    }

    /**
     * @return {RequestInit} the prepared request init
     */
    private simpleInit(): RequestInit {
        const options: RequestInit = {};
        options.headers = {'X-Requested-With': 'XMLHttpRequest'};
        return options;
    }

    /**
     * @param body {RequestInit['body']} the content to store
     * @return {RequestInit} the prepared request init
     */
    private storeInit( body: RequestInit['body'] ): RequestInit {
        const options: RequestInit = this.simpleInit();
        options.method = 'POST';
        options.body = body;
        return options;
    }

    /**
     * @param body {RequestInit['body']} the content to update
     * @return {RequestInit} the prepared request init
     */
    private updateInit( body: RequestInit['body'] ): RequestInit {
        const options: RequestInit = this.simpleInit();
        options.method = 'PUT';
        Object.assign( <Object>options.headers, { 'Content-Type': 'application/json' } );
        options.body = body;
        return options;
    }

    /**
     * @return RequestInit} the prepared request init
     */
    private deleteInit(): RequestInit {
        const options: RequestInit = this.simpleInit();
        options.method = 'DELETE';
        return options;
    }

    /**
     * @description find all the items of the service model from the database
     * @return the list of all items from the service model
     */
    async all(): Promise<T[]> {

        const response: ModelResponseArray<K[]> = await fetch( this.base_url, this.simpleInit() );
        if( !response.ok ) throw Error(response.statusText);

        const json: K[] = await response.json();
        return json.map<T>( data =>  new (this.blueprint)( data ) );

    }

    /**
     * @description find the corresponding item from the database and return it
     * @param id {number} the id of the item to fetch
     * @return the object model corresponding to id
     */
    async find( id: number ): Promise<T> {

        const response: ModelResponse<K> = await fetch( this.base_url + '/' + id, this.simpleInit() );
        if( !response.ok ) throw Error(response.statusText);

        const json = await response.json();
        return new (this.blueprint)( json )
        
    }

    /**
     * @description store the entry into database, then return the corresponding model
     * @param entry the entry data to store
     * @return {Promise<T>} the recently created object on database
     */
    async store( entry: ModelFormData<K> | HTMLFormElement ): Promise<T> {
        
        let data: ModelFormData<K>;
        if( (<any>entry.constructor).name == 'HTMLFormElement' )
            data = new FormData( <HTMLFormElement>entry );
        else
            data = <ModelFormData<K>>entry;
        

        const response: ModelResponse<K> = await fetch( this.base_url, this.storeInit( data ) );
        if( !response.ok ) throw Error(response.statusText);

        const json = await response.json();
        return new (this.blueprint)( json );
        
    }

    /**
     * @description update the entry into database, then return the corresponding model
     * @param id {number} the id of the item to update
     * @param entry the entry data to update
     * @return {Promise<T>} the recently updated object on database
     */
    async update( id: number, entry: K | HTMLFormElement ): Promise<T> {
        
        let data: string;
        if( (<any>entry.constructor).name == 'HTMLFormElement' ){
            var object = {};
            let form_data = new FormData( <HTMLFormElement>entry );
            form_data.forEach((value, key) => {(<any>object)[key] = value});
            data = JSON.stringify(object);
        }
        else 
            data = JSON.stringify( entry );

        const response: ModelResponse<K> = await fetch( this.base_url + '/' + id, this.updateInit( data ) );
        if( !response.ok ) throw Error(response.statusText);

        const json = await response.json();
        return new (this.blueprint)( json );
        
    }

    /**
     * @description delete the specified item from the database, then return the corresponding model
     * @param id {number} the id of the item to delete
     * @return {Promise<T>} the recently deleted object on database
     */
    async delete( id: number ): Promise<T> {

        const response: ModelResponse<K> = await fetch( this.base_url + '/' + id, this.deleteInit() );
        if( !response.ok ) throw Error(response.statusText);

        const json = await response.json();
        return new (this.blueprint)( json );

    }

}