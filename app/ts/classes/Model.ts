export abstract class Model {

    protected id: number;

    constructor( literal?: ModelLiteral ) {
        if( literal )
            this.id = literal.id;
    }

}

export interface ModelLiteral {

    id: number;

}